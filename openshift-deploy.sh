#!/bin/bash
set -x

source secrets

oc process -f openshift/ai-library-ui-seed.yml \
  -p S3_CONCURRENT_UPLOAD_LIMIT=${S3_CONCURRENT_UPLOAD_LIMIT} \
  -p GIT_REF=${GIT_REF} \
  | oc create -f -
